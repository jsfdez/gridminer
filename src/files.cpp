#include "files.h"

const std::string Files::k_imageBackground = "data/BackGround.jpg";
const std::string Files::k_imageGemBlue = "data/Blue.png";
const std::string Files::k_imageGemGreen = "data/Green.png";
const std::string Files::k_imageGemPurple = "data/Purple.png";
const std::string Files::k_imageGemRed = "data/Red.png";
const std::string Files::k_imageGemYellow = "data/Yellow.png";
const std::string Files::k_font = "data/FunnyKid.ttf";
const std::string Files::k_gameOverFont = "data/Gameover.ttf";

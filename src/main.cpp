#include <stack>
#include <memory>
#include <chrono>
#include <cstdint>
#include <cstdlib>

#include <SDL2/SDL.h>
#include "gamesurface.h"
#include "files.h"
#include "loader.h"

template<typename QuitFunction>
struct System
{
    std::int32_t status = -1;
    QuitFunction quitFunction;

    ~System() { Quit(); }

    void Quit()
    {
        if (status == 0) {
            quitFunction();
            status = -1;
        }
    }
};

template<typename QuitFunction>
auto InitSystem(std::int32_t status, QuitFunction quitFunction)
{
    return System<QuitFunction>{status, quitFunction};
}

int main(int argc SDL_UNUSED, char **argv SDL_UNUSED)
{
    auto sdl = InitSystem(SDL_Init(SDL_INIT_VIDEO), &SDL_Quit);
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> window(
                nullptr, &SDL_DestroyWindow);
    {
        auto background = loader::Image(Files::k_imageBackground);
        if (background) {
            window.reset(SDL_CreateWindow(
                             "Grid Miner by Jesus Fernandez (jsfdez@gmail.com). Press ESC to Quit",
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED,
                             background->w,
                             background->h,
                             SDL_WINDOW_OPENGL)
                         );
        } else {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                     "Grid Miner",
                                     "Cannot load data.",
                                     nullptr);
            return EXIT_FAILURE;
        }
    }

    if (!window) {
        printf("Could not create window: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    const auto ttf = InitSystem(TTF_Init(), &TTF_Quit);
    {
        auto& surface = *SDL_GetWindowSurface(window.get());
        struct _Scenes : std::stack<std::unique_ptr<AbstractSurface>>
        {
            AbstractSurface* operator->()
            {
                return top().get();
            }
        } scenes;
        std::srand(static_cast<unsigned int>(time(nullptr)));
        SDL_Event event;
        scenes.emplace(new GameSurface);
        while(!sdl.status) {
            auto startTime = std::chrono::system_clock::now();
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_KEYDOWN) {
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        sdl.Quit();
                }
                else scenes->Update(event);
            }
            if (!sdl.status) {
                scenes->Update(std::chrono::system_clock::now());
                scenes->Render(surface);
                SDL_UpdateWindowSurface(window.get());
                auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::system_clock::now() - startTime);
                const std::chrono::milliseconds minTime(1000 / 60);
                auto waitTime = minTime - elapsed;
                if (waitTime.count() > 0)
                    SDL_Delay(static_cast<std::uint32_t>(waitTime.count()));
            }
        }
    }
    return EXIT_SUCCESS;
}

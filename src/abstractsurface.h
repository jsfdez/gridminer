#pragma once

#include <chrono>

union SDL_Event;
struct Position;
struct SDL_Surface;

struct AbstractSurface
{
    virtual ~AbstractSurface() = default;

    enum class Status
    {
        Continue,
        Animation,
        Exit,
    };

    virtual Status Update(const SDL_Event &event) = 0;
    virtual Status Update(const std::chrono::time_point<std::chrono::system_clock> &time) = 0;

    virtual void Render(SDL_Surface &surface) = 0;
    virtual bool Contains(const Position &position) const = 0;
};

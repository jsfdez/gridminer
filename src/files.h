#pragma once

#include <string>

struct Files
{
    static const std::string k_imageBackground;
    static const std::string k_imageGemBlue;
    static const std::string k_imageGemGreen;
    static const std::string k_imageGemPurple;
    static const std::string k_imageGemRed;
    static const std::string k_imageGemYellow;
    static const std::string k_font;
    static const std::string k_gameOverFont;
};

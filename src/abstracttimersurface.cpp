#include "abstracttimersurface.h"

#include "constants.h"

void AbstractTimerSurface::Start()
{
    m_started = false;
    m_running = true;
}

void AbstractTimerSurface::Stop()
{
    m_running = m_started = false;
}

AbstractTimerSurface::Status AbstractTimerSurface::Update(
        const std::chrono::time_point<std::chrono::system_clock> &time)
{
    auto ret = Status::Continue;
    if (m_running) {
        if (!m_started) {
            m_startTime = time;
            m_started = true;
        }
        else {
            const auto diff = time - m_startTime;
            const auto s = std::chrono::duration_cast<std::chrono::seconds>(diff);
            if (s.count() >= Constants::MaxTime) {
                Stop();
                ret = Status::Exit;
            }
        }
    }
    return ret;
}

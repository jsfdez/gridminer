#include "gamesurface.h"

#include <chrono>
#include <cstdint>
#include <algorithm>
#include <functional>

#include <SDL2/SDL.h>
#include "files.h"
#include "loader.h"
#include <SDL2/SDL_ttf.h>
#include "gamesurface.h"

std::uint8_t k_noGem = std::numeric_limits<std::uint8_t>::max();

GameSurface::GameSurface()
    : m_candlewick(*this)
    , m_scoreboard(*this)
    , m_gameOverFont(nullptr, TTF_CloseFont)
    , m_gameOverSurface(nullptr, SDL_FreeSurface)
{
    Create();
}

GameSurface::~GameSurface()
{
    Destroy();
}

AbstractSurface::Status GameSurface::Update(const SDL_Event &event)
{
    auto status = Status::Continue;

    if (m_gameOver)
    {
        if (event.type == SDL_MOUSEBUTTONDOWN)
        {
            Destroy();
            Create();
            status = Status::Continue;
        }
        else  status = Status::Exit;
    }
    else if (m_animation == Animation::NoAnimation) switch (event.type)
    {
    case SDL_MOUSEMOTION:
        OnMouseMoveEvent(event);
        break;
    case SDL_MOUSEBUTTONDOWN:
        OnMouseClickEvent(event);
        break;
    case SDL_MOUSEBUTTONUP:
        OnMouseReleaseEvent(event);
        break;
    }
    return status;
}

AbstractSurface::Status GameSurface::Update(
        const std::chrono::time_point<std::chrono::system_clock> &time)
{
    Status status = AbstractSurface::Status::Continue;
    for (auto &gem : m_gems)
    {
        if (gem.Update(time) == AbstractSurface::Status::Animation)
        {
            status = AbstractSurface::Status::Animation;
        }
    }
    if (status == AbstractSurface::Status::Continue)
    {
        if (m_animation == Animation::FallAnimation)
        {
            m_animation = Animation::NoAnimation;
        }
        else if (m_animation == Animation::SwappingAnimation)
        {
            SDL_assert(m_swapping.first != m_swapping.second);
            auto groups = FindGroups();
            m_destroyingGems.swap(groups);
            if (m_destroyingGems.empty())
            {
                Swap(m_swapping.first, m_swapping.second, true);
            }
            else StartDestruction();
        }
        else if (m_animation == Animation::DestroyAnimation)
        {
            while(!FillRow());
            auto groups = FindGroups();
            m_destroyingGems.swap(groups);
            if (!m_destroyingGems.empty())
                StartDestruction();
        }
        else if (m_animation == Animation::RollbackAnimation)
        {
            SDL_assert(m_swapping.first != m_swapping.second);
#ifdef _DEBUG
            auto &&groups = FindGroups();
            SDL_assert(groups.empty());
#endif
            m_animation = Animation::NoAnimation;
            m_selectedGem = k_noGem;
        }
    }
    {
        m_candlewick.Update(time);
        if (m_scoreboard.Update(time) == AbstractSurface::Status::Exit)
        {
            m_gameOver = true;
        }
    }
    return status;
}

void GameSurface::Render(SDL_Surface &surface)
{
    SDL_BlitSurface(m_background.get(), nullptr, &surface, nullptr);
    const auto bind = std::bind(&GemSurface::Render, std::placeholders::_1,
                                std::ref(surface));
    std::vector<GemSurface*> postponed;
    for (auto& gem : m_gems)
    {
        if (!gem.IsDragging())
            gem.Render(surface);
        else
            postponed.push_back(&gem);
    }
    std::for_each(postponed.begin(), postponed.end(), bind);
    m_candlewick.Render(surface);
    m_scoreboard.Render(surface);
    if (m_gameOver)
    {
        auto pixels = static_cast<std::uint32_t*>(surface.pixels);
        SDL_assert(surface.format->BytesPerPixel == 4);
        for (auto i = 0; i < surface.w * surface.h; ++i)
        {
            auto f = surface.format;
            std::uint8_t r, g, b, a;
            SDL_GetRGBA(pixels[i], surface.format, &r, &g, &b, &a);
            r = static_cast<std::uint16_t>(r + 0x20) > 0xFF ? 0xFF : r + 0x20;
            g = static_cast<std::uint16_t>(r + 0x20) > 0xFF ? 0xFF : g + 0x20;
            b = static_cast<std::uint16_t>(r + 0x20) > 0xFF ? 0xFF : b + 0x20;
            pixels[i] = r << f->Rshift | g << f->Gshift | g << f->Bshift
                             | a << f->Ashift;
        }

        if (!m_gameOverSurface)
        {
            m_gameOverFont.reset(TTF_OpenFont(Files::k_gameOverFont.c_str(), 100));
            SDL_assert(m_gameOverFont);
            m_gameOverSurface.reset(TTF_RenderText_Solid(m_gameOverFont.get(),
                                                         "GAME OVER!", SDL_Color{ 0x20, 0x20, 0x20, 0xFF }));
            SDL_assert(m_gameOverSurface);
        }
        SDL_Rect rect{
            surface.w / 2 - m_gameOverSurface->w / 2,
                    surface.h / 2 - m_gameOverSurface->h / 2,
                    m_gameOverSurface->w,
                    m_gameOverSurface->h,
        };
        SDL_BlitSurface(m_gameOverSurface.get(), nullptr, &surface, &rect);
    }
}

void GameSurface::Create()
{
    m_background = loader::Image(Files::k_imageBackground);
    decltype(FindGroups()) sets;
    m_selectedGem = k_noGem;
    std::size_t times = 0;
    do
    {
        // i = gems.size() to avoid unnecesary clears
        for (auto i = m_gems.size(); i < GemCount; ++i)
        {
            auto position = CalculateGemPosition(i);
            position.X = 0;
            position.Y = -OffsetY - position.Y;
            m_gems.emplace_back(*this, position);
        }
        auto groups = FindGroups();
        sets.swap(groups);
        ++times;
        for (auto& set : sets) // Randomize found sets
        {
            for (auto& i : set)
            {
                auto color = static_cast<GemColor>(
                            std::rand() % GemSurface::ColorCount);
                m_gems[i].SetColor(color);
            }
        }
    } while (!sets.empty());
    m_animation = Animation::FallAnimation;
    SDL_Log("Acceptable grid found in %lu iterations", times);

    m_candlewick.Start();
    m_scoreboard.Start();
    m_gameOver = false;
}

void GameSurface::Destroy()
{
    m_gameOverSurface.reset();
    m_gameOverFont.reset();
    m_gems.clear();
    m_candlewick.Stop();
    m_scoreboard.Stop();
    m_background.reset();
}

std::set<std::vector<std::uint8_t>> GameSurface::FindGroups() const
{
    decltype(FindGroups()) ret;
    typedef std::function<void(std::vector<std::uint8_t> &)> MatchFunction;
    const MatchFunction matchLeft = [&](std::vector<std::uint8_t> &data)
    {
        auto last = data.back();
        auto next = last - 1;
        if ((last % Columns) != 0
                && m_gems[last].GetColor() == m_gems[next].GetColor())
        {
            data.push_back(last - 1);
            matchLeft(data);
        }
    };
    const MatchFunction matchRight = [&](std::vector<std::uint8_t> &data)
    {
        auto last = data.back();
        auto next = last + 1;
        if ((last % Columns) != Columns - 1
                && m_gems[last].GetColor() == m_gems[next].GetColor())
        {
            data.push_back(last + 1);
            matchRight(data);
        }
    };
    const MatchFunction matchUp = [&](std::vector<std::uint8_t> &data)
    {
        auto last = data.back();
        auto next = last - Columns;
        if ((last / Columns) != 0
                && m_gems[last].GetColor() == m_gems[next].GetColor())
        {
            data.push_back(last - Columns);
            matchUp(data);
        }
    };
    const MatchFunction matchDown = [&](std::vector<std::uint8_t> &data)
    {
        auto last = data.back();
        auto next = last + Columns;
        if (last + Columns < GemCount
                && m_gems[last].GetColor() == m_gems[next].GetColor())
        {
            data.push_back(last + Columns);
            matchDown(data);
        }
    };
    const auto size = m_gems.size();
    for (std::uint8_t i = 0u; i < size; ++i)
    {
        std::vector<std::uint8_t> horizontal{ i }, vertical{ i };
        matchLeft(horizontal);
        std::reverse(horizontal.begin(), horizontal.end());
        matchRight(horizontal);

        matchUp(vertical);
        std::reverse(vertical.begin(), vertical.end());
        matchDown(vertical);

        if (horizontal.size() >= 3)
            ret.insert(horizontal);
        if (vertical.size() >= 3)
            ret.insert(vertical);
    }
    return ret;
}

void GameSurface::OnMouseMoveEvent(const SDL_Event &event)
{
    auto& e = reinterpret_cast<const SDL_MouseMotionEvent &>(event);
    Position position;
    position.X = e.x;
    position.Y = e.y;
    if (e.state == SDL_BUTTON_LEFT)
    {
        auto gemPosition = CalculateGemPosition(m_selectedGem);
        if (m_selectedGem != k_noGem
                && (SDL_abs(gemPosition.X - position.X) > GemSurface::Width / 2
                    || SDL_abs(gemPosition.Y - position.Y) > GemSurface::Height / 2))
        {
            gemPosition.X = position.X - gemPosition.X;
            gemPosition.Y = position.Y - gemPosition.Y;
            m_gems[m_selectedGem].SetOffset(gemPosition);
            m_gems[m_selectedGem].SetDragging(true);
        }
    }
    for (auto& child : m_gems)
    {
        child.SetHover(false);
        if (child.Contains(position))
        {
            child.SetHover(true);
        }
    }
}

void GameSurface::OnMouseClickEvent(const SDL_Event &event)
{
    auto& e = reinterpret_cast<const SDL_MouseButtonEvent &>(event);
    Position position;
    position.X = e.x;
    position.Y = e.y;
    for (auto i = 0u; i < m_gems.size(); ++i)
    {
        auto& child = m_gems[i];
        if (m_selectedGem != k_noGem)
        {
            if (child.Contains(position))
            {
                if (AreContiguous(m_selectedGem, i))
                {
                    Swap(m_selectedGem, i, false);
                    m_selectedGem = k_noGem;
                }
                else
                {
                    m_selectedGem = i;
                }
            }
        }
        else if (child.Contains(position))
        {
            m_selectedGem = i;
        }
    }
}

void GameSurface::OnMouseReleaseEvent(const SDL_Event &event)
{
    if (m_selectedGem != k_noGem)
    {
        if (m_gems[m_selectedGem].IsDragging())
        {
            m_gems[m_selectedGem].SetDragging(false);
            OnMouseClickEvent(event);
        }
    }
}

bool GameSurface::Contains(const Position &position SDL_UNUSED) const
{
    return true;
}

bool GameSurface::AreContiguous(std::uint8_t first, std::uint8_t second) const
{
    if (first == second)
        return false;
    if (first / Columns == second / Columns)
        return first + 1 == second || first - 1 == second;
    else
        return SDL_abs(first - second) == Columns;
}

std::uint8_t GameSurface::GetGemIndex(const GemSurface &gem) const
{
    decltype(m_gems.size()) size = m_gems.size();
    for (decltype(size) i = 0; i < size; ++i)
    {
        if (&m_gems[i] == &gem)
        {
            return static_cast<decltype(GetGemIndex(gem))>(i);
        }
    }
    return std::numeric_limits<decltype(GetGemIndex(gem))>::max();
}

Position GameSurface::CalculateGemPosition(std::uint8_t i) const
{
    Position position;
    position.X = OffsetX + GemSurface::Width * (i % Columns);
    position.Y = OffsetY + GemSurface::Height * (i / Columns);
    return position;
}

void GameSurface::Swap(std::uint8_t first, std::uint8_t second, bool rollback)
{
    GemSurface::StartSwapping(m_gems[first], m_gems[second]);
    m_swapping = std::make_pair(first, second);
    m_animation = rollback ? Animation::RollbackAnimation
                           : Animation::SwappingAnimation;
}

bool GameSurface::FillRow()
{
    const auto findAboveGem = [&](decltype(m_gems.size()) gemIndex)
    {
        for (int i = gemIndex - Columns; i >= 0; i -= Columns)
        {
            if (m_gems[i].GetColor() != GemColor::Empty)
            {
                return static_cast<decltype(k_noGem)>(i);
            }
        }
        return k_noGem;
    };
    bool nextRow = true;
    for (auto row = 0; row < Rows && nextRow; ++row)
    {
        for (auto column = 0; column < Columns; ++column)
        {
            const auto currentRow = Rows - row - 1;
            auto index = column + currentRow * Columns;
            if (m_gems[index].IsEmpty())
            {
                auto otherIndex = findAboveGem(index);
                auto position = CalculateGemPosition(index);
                position.X = 0;
                if (otherIndex != k_noGem)
                {
                    position.Y = -(position.Y - CalculateGemPosition(otherIndex).Y);
                    std::swap(m_gems[index], m_gems[otherIndex]);
                    m_gems[index].SetOffset(position);
                }
                else
                {
                    position.Y = -OffsetY - position.Y;
                    GemSurface gem(*this, position);
                    m_gems[index] = gem;
                    m_gems[index].SetOffset(position);
                }
                m_animation = Animation::FallAnimation;
                nextRow = false;
            }
        }
    }
    return nextRow;
}

void GameSurface::StartDestruction()
{
    for (auto& group : m_destroyingGems)
    {
        SDL_assert(group.size() >= 3);
        auto diff = group[1] - group[0];
        for (auto& i : group)
        {
            m_scoreboard.AddScore(10);
            m_gems[i].Destroy(diff == 1
                              ? GemSurface::Destruction::Horizontal
                              : GemSurface::Destruction::Vertical);
        }
    }
    m_animation = Animation::DestroyAnimation;
    m_swapping.first = m_swapping.second;
    m_selectedGem = k_noGem;
}

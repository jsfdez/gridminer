#pragma once

#include "abstractsurface.h"

#include <set>
#include <map>
#include <vector>
#include <memory>

#include "gemsurface.h"
#include "candlewicksurface.h"
#include "scoreboardsurface.h"

struct Position;
struct SDL_MouseMotionEvent;
struct SDL_MouseButtonEvent;

class GameSurface : public AbstractSurface
{
    enum
    {
        Rows = 8,
        Columns = Rows,
        GemCount = Rows * Columns,
        OffsetX = 330u + GemSurface::Width / 2,
        OffsetY = 100u + GemSurface::Height / 2,
    };

    enum class Animation
    {
        NoAnimation,
        FallAnimation,
        SwappingAnimation,
        RollbackAnimation,
        DestroyAnimation,
    } m_animation = Animation::NoAnimation;

    std::shared_ptr<SDL_Surface> m_background;
    std::vector<GemSurface> m_gems;
    std::uint8_t m_selectedGem;
    std::pair<std::uint8_t, std::uint8_t> m_swapping;
    std::set<std::vector<std::uint8_t>> m_destroyingGems;
    CandlewickSurface m_candlewick;
    ScoreboardSurface m_scoreboard;
    bool m_gameOver = false;
    std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> m_gameOverFont;
    std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)> m_gameOverSurface;

    enum class Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
    };
    std::set<std::vector<std::uint8_t>> FindGroups() const;
    bool AreContiguous(std::uint8_t first, std::uint8_t second) const;
    void Swap(std::uint8_t first, std::uint8_t second, bool rollback);
    bool FillRow();
    void StartDestruction();

    void OnMouseMoveEvent(const SDL_Event &event);
    void OnMouseClickEvent(const SDL_Event &event);
    void OnMouseReleaseEvent(const SDL_Event &event);

public:
    GameSurface();
    ~GameSurface() override;

    virtual Status Update(const SDL_Event &event) override;

    virtual Status Update(const std::chrono::time_point<std::chrono::system_clock> &time) override;

    virtual void Render(SDL_Surface &surface) override;
    virtual bool Contains(const Position &position) const override;

    std::uint8_t GetGemIndex(const GemSurface &gem) const;
    Position CalculateGemPosition(std::uint8_t i) const;

    void Create();
    void Destroy();
};

#pragma once

namespace Constants
{

enum
{
    MaxTime = 60, // seconds

    TitleX = 500,
    TitleY = 85,

    Spacing = 20,

    TimeTitleX = 130,
    TimeTitleY = 200,
    TimeX = TimeTitleX,
    TimeY = TimeTitleY + Spacing,

    ScoreTitleX = TimeTitleX,
    ScoreTitleY = 50,
    ScoreX = ScoreTitleX,
    ScoreY = ScoreTitleY + Spacing,
};

}

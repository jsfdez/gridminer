#pragma once

#include <cstdint>
#include <limits>

enum class GemColor : std::uint8_t
{
    Blue = 0,
    Green,
    Purple,
    Red,
    Yellow,

    Empty = std::numeric_limits<decltype(GemColor::Blue)>::max()
};
